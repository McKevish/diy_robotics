######################### GENERATE KEY FOR GITLAB ########################
# For more information read: https://docs.gitlab.com/ee/ssh/

# Login at: https://gitlab.com/users/sign_in
# Go to: profile-->settings-->ssh keys   (https://gitlab.com/profile/keys)

# Create a public and private key on your computer
$ ssh-keygen -t ed25519 -C "<Email>"
$ gedit ~/.ssh/id_ed25519.p
# Copy the public key and close gedit
# paste your public key in the text box in gitlab
# click on the 'Add key'

#################### GET THE DIY ROBOTICS REPOSITORY #####################
# install git on your computer if not installed
$ sudo apt install git

# create a directory for the repository
$ mkdir Development
$ cd Development

# setup your git username and email
$ git config --global user.name "<Username>"
$ git config --global user.email "<Email>"

# clone the repository
$ git clone git@gitlab.com:McKevish/diy_robotics.git

######################## DESKTOP ROS INSTALLATION ########################

# make sure that the desktop ros 2 installation script has executable permissions
$ chmod +x /home/$USER/Development/diy_robotics/desktop_ros_installation.sh

# start the script to install ros2 on your desktop
. /home/kevin/Development/diy_robotics/desktop_ros_installation.sh 

# check if the installation worked by folowing this tutorial
https://index.ros.org/doc/ros2/Tutorials/Colcon-Tutorial/


######################## INSTALL DOCKER #####################

# install curl to install docker on your desktop
$ sudo apt install curl

## install docker on your computer
$ sudo apt install docker.io

# check if docker is installed by checking the version number
$ docker --version

## install docker compose on your computer
# find the 'latest release' at: https://github.com/docker/compose/releases

$ sudo curl -L https://github.com/docker/compose/releases/download/1.24.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose

# check if docker compose is installed by checking the version number
$ docker-compose --version

## install docker machine on your computer
# find the 'latest release' at: https://github.com/docker/machine/releases

$ sudo curl -L https://github.com/docker/machine/releases/download/v0.16.1/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine
$ sudo chmod +x /tmp/docker-machine
$ sudo cp /tmp/docker-machine /usr/local/bin/docker-machine

# check if docker machine is installed by checking the version number
$ docker-machine --version

## solve docker permission issue (when you dont want to add sudo before every docker command)
# please read the following link for more information: https://docs.docker.com/install/linux/linux-postinstall/

$ sudo groupadd docker
$ sudo usermod -aG docker $USER
$ newgrp docker 
