FROM 	ubuntu:18.04

RUN 	apt-get clean && apt-get update && apt-get install -y locales

RUN 	apt-get install -y tzdata
CMD 	date

RUN	locale-gen en_US en_US.UTF-8 && \
	update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 && \
	export LANG=en_US.UTF-8

RUN 	apt-get update && \
	apt install -y curl gnupg2 lsb-release && \
	curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add - && \
	sh -c 'echo "deb [arch=amd64,arm64] http://packages.ros.org/ros2/ubuntu `lsb_release -cs` main" > /etc/apt/sources.list.d/ros2-latest.list'

RUN 	apt-get update && \ 
	apt-get install -y \
  	build-essential \
  	cmake \
  	git \
  	python3-colcon-common-extensions \
  	python3-pip \
  	python-rosdep \
  	python3-vcstool \
  	wget

RUN	python3 -m pip install -U \
 	argcomplete \
  	flake8 \
  	flake8-blind-except \
  	flake8-builtins \
  	flake8-class-newline \
  	flake8-comprehensions \
  	flake8-deprecated \
  	flake8-docstrings \
  	flake8-import-order \
  	flake8-quotes \
  	pytest-repeat \
  	pytest-rerunfailures \
  	pytest \
  	pytest-cov \
  	pytest-runner \
  	setuptools

RUN 	apt-get install --no-install-recommends -y \
  	libasio-dev \
  	libtinyxml2-dev

RUN	git clone https://gitlab.com/McKevish/diy_robotics.git
RUN	mkdir -p /ros2_ws/src
WORKDIR	/ros2_ws
RUN	vcs import src < ../diy_robotics/ros2.repos

RUN 	rosdep init && \
	rosdep update && \
	rosdep install --from-paths src --ignore-src --rosdistro dashing -y --skip-keys "console_bridge fastcdr fastrtps libopensplice67 libopensplice69 rti-connext-dds-5.3.1 urdfdom_headers"

RUN 	apt install -y libopensplice69
RUN	colcon build --symlink-install





